﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackupManger
{
    class Program
    {
        static void Main(string[] args)
        {
            var datestring = DateTime.Now.ToString("ddMMyyyy-HHmm");
            string mongodumppath = ConfigurationSettings.AppSettings["mongodumppath"];
            string mongoServer = ConfigurationSettings.AppSettings["mongoServer"];
            string mongoUserName = ConfigurationSettings.AppSettings["mongoUserName"];
            string mongoPassword = ConfigurationSettings.AppSettings["mongoPassword"];
            string mongoDB = ConfigurationSettings.AppSettings["mongoDB"];
            string path = ConfigurationSettings.AppSettings["backupfilepath"] + datestring;
            System.IO.Directory.CreateDirectory(path);
            string commandToExecute = @"""" + mongodumppath + @""" -h " + mongoServer + " -d " + mongoDB + " -u " + mongoUserName + " -p " + mongoPassword + " -o " + path;
            Process.Start(@"cmd", @"/c " + commandToExecute);

        }
    }
}
